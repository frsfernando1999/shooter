// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"

#include "BrainComponent.h"
#include "EnemyController.h"
#include "MainCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Blueprint/UserWidget.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Sound/SoundCue.h"

// Sets default values
AEnemy::AEnemy():
	Health(100.f),
	MaxHealth(100.f),
	HeadBone(FString("head")),
	HealthBarDisplayTime(5.f),
	bCanHitReact(true),
	HitReactTimeMin(0.5f),
	HitReactTimeMax(1.5f),
	DamageNumberDestroyTime(1.5f),
	StunChance(0.2f),
	AttackLFast(TEXT("AttackLFast")),
	AttackRFast(TEXT("AttackRFast")),
	AttackL(TEXT("AttackL")),
	AttackR(TEXT("AttackR")),
	BaseDamage(30.f),
	bCanAttack(true),
	AttackWaitTime(1.f),
	DeathTime(4.f),
	RootBone(FName("root"))
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AggroSphere = CreateDefaultSubobject<USphereComponent>(TEXT("AggroSphere"));
	AggroSphere->SetupAttachment(GetRootComponent());

	InRangeSphere = CreateDefaultSubobject<USphereComponent>(TEXT("InRangeSphere"));
	InRangeSphere->SetupAttachment(GetRootComponent());

	LeftWeaponCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("LeftWeaponBox"));
	LeftWeaponCollision->SetupAttachment(GetMesh(), FName("left_weapon_bone"));

	RightWeaponCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("RightWeaponBox"));
	RightWeaponCollision->SetupAttachment(GetMesh(), FName("right_weapon_bone"));
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	AggroSphere->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::AggroSphereOverlap);
	AggroSphere->OnComponentEndOverlap.AddDynamic(this, &AEnemy::AggroSphereEndOverlap);

	InRangeSphere->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::InCombatRangeSphereOverlap);
	InRangeSphere->OnComponentEndOverlap.AddDynamic(this, &AEnemy::InCombatRangeEndSphereOverlap);

	LeftWeaponCollision->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::OnLeftWeaponOverlap);
	LeftWeaponCollision->OnComponentEndOverlap.AddDynamic(this, &AEnemy::OnLeftWeaponEndOverlap);

	RightWeaponCollision->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::OnRightWeaponOverlap);
	RightWeaponCollision->OnComponentEndOverlap.AddDynamic(this, &AEnemy::OnRightWeaponEndOverlap);

	LeftWeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	LeftWeaponCollision->SetCollisionObjectType(ECC_WorldDynamic);
	LeftWeaponCollision->SetCollisionResponseToAllChannels(ECR_Ignore);
	LeftWeaponCollision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);

	RightWeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RightWeaponCollision->SetCollisionObjectType(ECC_WorldDynamic);
	RightWeaponCollision->SetCollisionResponseToAllChannels(ECR_Ignore);
	RightWeaponCollision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);

	GetMesh()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);

	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);

	EnemyController = Cast<AEnemyController>(GetController());
	if (EnemyController)
	{
		EnemyController->GetBlackboardComponent()->SetValueAsBool(TEXT("CanAttack"), true);
	}
	SetPatrolPoint();
	Health = MaxHealth;
}

void AEnemy::SetPatrolPoint() const
{
	// Transforms Local Space to World Space
	const FVector WorldPatrolPoint = UKismetMathLibrary::TransformLocation(GetActorTransform(), PatrolPoint);
	const FVector WorldPatrolPoint2 = UKismetMathLibrary::TransformLocation(GetActorTransform(), PatrolPoint2);
	//

	if (EnemyController)
	{
		EnemyController->GetBlackboardComponent()->SetValueAsVector(TEXT("PatrolPoint"), WorldPatrolPoint);
		EnemyController->GetBlackboardComponent()->SetValueAsVector(TEXT("PatrolPoint2"), WorldPatrolPoint2);

		EnemyController->RunBehaviorTree(BehaviorTree);
	}
}

void AEnemy::ShowHealthBar_Implementation()
{
	GetWorldTimerManager().ClearTimer(HealthBarTimer);
	GetWorldTimerManager().SetTimer(HealthBarTimer, this, &AEnemy::HideHealthBar, HealthBarDisplayTime);
}

void AEnemy::Die()
{
	if (bDying) return;
	bDying = true;
	HideHealthBar();

	if (DeathMontage)
	{
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance)
		{
			AnimInstance->Montage_Play(DeathMontage);
		}
	}
}

void AEnemy::PlayHitMontage(const FName Section, const float PlayRate)
{
	if (bCanHitReact)
	{
		SetStunned(true);
		if (HitMontage)
		{
			UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
			if (AnimInstance)
			{
				AnimInstance->Montage_Play(HitMontage, PlayRate);
				AnimInstance->Montage_JumpToSection(Section, HitMontage);
			}
			bCanHitReact = false;
			const float HitReactTime = FMath::FRandRange(HitReactTimeMin, HitReactTimeMax);
			GetWorldTimerManager().SetTimer(HitReactTimer, this, &AEnemy::ResetHitReactTimer, HitReactTime);
		}
	}
}

void AEnemy::ResetHitReactTimer()
{
	bCanHitReact = true;
}

void AEnemy::DestroyDamageNumber(UUserWidget* DamageNumber)
{
	DamageNumbers.Remove(DamageNumber);
	DamageNumber->RemoveFromParent();
}

void AEnemy::UpdateHitNumbers()
{
	for (const auto& DamagePair : DamageNumbers)
	{
		UUserWidget* DamageNumber{DamagePair.Key};
		const FVector Location{DamagePair.Value};
		FVector2D ScreenPosition;

		UGameplayStatics::ProjectWorldToScreen(
			GetWorld()->GetFirstPlayerController(),
			Location,
			ScreenPosition
		);
		DamageNumber->SetPositionInViewport(ScreenPosition);
	}
}

void AEnemy::AggroSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                const FHitResult& SweepResult)
{
	if (OtherActor == nullptr) return;

	if (Cast<AMainCharacter>(OtherActor))
	{
		Character = Cast<AMainCharacter>(OtherActor);
		bInAggroRange = true;
	}
}

void AEnemy::AggroSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                   UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor == nullptr) return;

	if (Cast<AMainCharacter>(OtherActor))
	{
		bInAggroRange = false;
	}
}

void AEnemy::InCombatRangeSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                        UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                        const FHitResult& SweepResult)
{
	if (OtherActor == nullptr) return;

	if (Cast<AMainCharacter>(OtherActor))
	{
		bInAttackRange = true;
		if (EnemyController)
		{
			EnemyController->GetBlackboardComponent()->SetValueAsBool(TEXT("InAttackRange"), true);
		}
	}
}

void AEnemy::InCombatRangeEndSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                           UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor == nullptr) return;

	if (Cast<AMainCharacter>(OtherActor))
	{
		bInAttackRange = false;
		if (EnemyController)
		{
			EnemyController->GetBlackboardComponent()->SetValueAsBool(TEXT("InAttackRange"), false);
		}
	}
}

void AEnemy::OnLeftWeaponOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                 UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                 const FHitResult& SweepResult)
{
	if (AMainCharacter* MainCharacter = Cast<AMainCharacter>(OtherActor))
	{
		DoDamage(MainCharacter);
		SpawnBloodParticles(MainCharacter, OtherComp->GetComponentLocation());
	}
}

void AEnemy::OnLeftWeaponEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AEnemy::StunCharacter(AMainCharacter* MainCharacter) const
{
	if (FMath::RandRange(0.f, 1.f) < MainCharacter->GetStunChance())
	{
		MainCharacter->Stun();
	}
}

void AEnemy::OnRightWeaponOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                  UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                  const FHitResult& SweepResult)
{
	if (AMainCharacter* MainCharacter = Cast<AMainCharacter>(OtherActor))
	{
		DoDamage(MainCharacter);
		SpawnBloodParticles(MainCharacter, OtherComp->GetComponentLocation());
		StunCharacter(MainCharacter);
	}
}

void AEnemy::OnRightWeaponEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                     UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AEnemy::DoDamage(AMainCharacter* Victim)
{
	if (Victim == nullptr) return;

	UGameplayStatics::ApplyDamage(Victim, BaseDamage, EnemyController, this, UDamageType::StaticClass());
	if (Victim->GetMeleeImpactSound())
	{
		UGameplayStatics::PlaySoundAtLocation(this, Victim->GetMeleeImpactSound(), GetActorLocation());
	}
}

void AEnemy::SpawnBloodParticles(const AMainCharacter* Victim, FVector Location) const
{
	if (Victim->GetBloodParticles())
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Victim->GetBloodParticles(),
		                                         Location + FVector(10.f, 0.f, 20.f));
	}
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UpdateHitNumbers();
	SetAggro();
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

FName AEnemy::GetRandomMontageSection()
{
	switch (UKismetMathLibrary::RandomInteger(3))
	{
	case 0:
		return FName("HitReactFront");
	case 1:
		return FName("HitReactBack");
	case 2:
		return FName("HitReactRight");
	case 3:
		return FName("HitReactLeft");
	default:
		return FName("HitReactBack");
	}
}

void AEnemy::SetStunned(bool Stunned)
{
	if (EnemyController)
	{
		EnemyController->GetBlackboardComponent()->SetValueAsBool(TEXT("Stunned"), Stunned);
	}
}

void AEnemy::PlayAttackMontage(FName Section, float PlayRate)
{
	if (AttackMontage)
	{
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance)
		{
			AnimInstance->Montage_Play(AttackMontage, PlayRate);
			AnimInstance->Montage_JumpToSection(Section, AttackMontage);
		}
	}
	bCanAttack = false;
	GetWorldTimerManager().SetTimer(AttackWaitTimer, this, &AEnemy::ResetCanAttack, AttackWaitTime);
	if (EnemyController)
	{
		EnemyController->GetBlackboardComponent()->SetValueAsBool(TEXT("CanAttack"), false);
	}
}

FName AEnemy::GetAttackSectionName() const
{
	switch (UKismetMathLibrary::RandomInteger(3))
	{
	case 0:
		return AttackL;
	case 1:
		return AttackR;
	case 2:
		return AttackLFast;
	case 3:
		return AttackRFast;
	default:
		return AttackLFast;
	}
}

void AEnemy::SetAggro()
{
	if (bInAggroRange)
	{
		if (EnemyController->LineOfSightTo(Character))
		{
			EnemyController->GetBlackboardComponent()->SetValueAsObject(TEXT("Target"), Character);
		}
	}
	else
	{
		if (!EnemyController->LineOfSightTo(Character))
		{
			EnemyController->GetBlackboardComponent()->SetValueAsObject(TEXT("Target"), nullptr);
			if (Character)
			{
				EnemyController->GetBlackboardComponent()->SetValueAsVector(
					TEXT("LastKnownPlayerLocation"), Character->GetActorLocation());
			}
			Character = nullptr;
		}
	}
}

void AEnemy::ActivateLeftWeapon() const
{
	LeftWeaponCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void AEnemy::DeactivateLeftWeapon() const
{
	LeftWeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AEnemy::ActivateRightWeapon() const
{
	RightWeaponCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void AEnemy::DeactivateRightWeapon() const
{
	RightWeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AEnemy::ResetCanAttack()
{
	bCanAttack = true;
	if (EnemyController)
	{
		EnemyController->GetBlackboardComponent()->SetValueAsBool(TEXT("CanAttack"), true);
	}
}

void AEnemy::FinishDeath()
{
	GetMesh()->bPauseAnims = true;
	GetWorldTimerManager().SetTimer(
		DeathTimer, this, &AEnemy::DestroyEnemy, DeathTime);
}

void AEnemy::DestroyEnemy()
{
	if (DeathParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, DeathParticles, GetMesh()->GetBoneLocation(RootBone));
	}
	Destroy();
}

void AEnemy::BulletHit_Implementation(FHitResult HitResult, AActor* Shooter, AController* MainInstigator)
{
	IBulletHitInterface::BulletHit_Implementation(HitResult, Shooter, MainInstigator);

	if (ImpactSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ImpactSound, GetActorLocation());
	}
	if (ImpactParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactParticles, HitResult.Location, FRotator(0.f), true);
	}
}

void AEnemy::DisablesMeshAfterDeath() const
{
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Ignore);

	LeftWeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RightWeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Ignore);
}

float AEnemy::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
                         AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (Health - DamageAmount <= 0.f)
	{
		(Health = 0);
		if (EnemyController)
		
		{
			EnemyController->GetBlackboardComponent()->SetValueAsBool(FName("Dead"), true);
			EnemyController->StopMovement();
		}

		DisablesMeshAfterDeath();
		EnemyController->BrainComponent->StopLogic(TEXT("Player is Dead"));
		Die();
	}
	else
	{
		(Health -= DamageAmount);
	}

	if (EnemyController && !bDying)
	{
		if (AMainCharacter* MainCharacter = Cast<AMainCharacter>(DamageCauser))
		{
			EnemyController->GetBlackboardComponent()->SetValueAsObject(FName("Target"), DamageCauser);
			Character = MainCharacter;
		}
	}

	if (bDying) return 0.f;

	ShowHealthBar();

	const float StunRange = FMath::RandRange(0.f, 1.f);
	if (StunRange <= StunChance)
	{
		PlayHitMontage(GetRandomMontageSection());
	}

	return DamageAmount;
}

void AEnemy::StoreDamageNumber(UUserWidget* DamageNumber, FVector Location)
{
	DamageNumbers.Add(DamageNumber, Location);
	FTimerHandle DamageNumberTimer;
	FTimerDelegate DamageNumberDelegate;

	//Binds Functions and receives it's arguments as parameters        
	DamageNumberDelegate.BindUFunction(this, FName("DestroyDamageNumber"), DamageNumber);
	//														^^^^^FunctionName^^^^^, ^^Parameter^^

	GetWorld()->GetTimerManager().SetTimer(DamageNumberTimer,
	                                       DamageNumberDelegate,
	                                       DamageNumberDestroyTime,
	                                       false);
}
