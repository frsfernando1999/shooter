// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyController.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_API AEnemyController : public AAIController
{
	GENERATED_BODY()
	
public:
	
	AEnemyController();
	virtual void OnPossess(APawn* InPawn) override;

	FORCEINLINE UBlackboardComponent* GetBlackboardComponent() const {return BlackboardComponent; }
	
private:
	UPROPERTY(BlueprintReadWrite, Category="AI", meta=(AllowPrivateAccess="true"))
	UBlackboardComponent* BlackboardComponent;

	UPROPERTY(BlueprintReadWrite, Category="AI", meta=(AllowPrivateAccess="true"))
	class UBehaviorTreeComponent* BehaviorTreeComponent;

};
