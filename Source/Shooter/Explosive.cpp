// Fill out your copyright notice in the Description page of Project Settings.


#include "Explosive.h"

#include "Enemy.h"
#include "MainCharacter.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

// Sets default values
AExplosive::AExplosive():
	bDestroyed(false),
	ExplosionDamage(50.f)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ExplosiveMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ExplosiveMesh"));
	SetRootComponent(ExplosiveMesh);

	OverlapSphere = CreateDefaultSubobject<USphereComponent>(TEXT("ExplosionSphere"));
	OverlapSphere->SetupAttachment(ExplosiveMesh);
}

// Called when the game starts or when spawned
void AExplosive::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AExplosive::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AExplosive::BulletHit_Implementation(FHitResult HitResult, AActor* Shooter, AController* MainInstigator)
{
	IBulletHitInterface::BulletHit_Implementation(HitResult, Shooter, MainInstigator);

	if (ExplosionSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ExplosionSound, GetActorLocation());
	}
	if (ExplosionParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionParticles, GetActorLocation(), FRotator(0.f),
		                                         true);
	}

	Explode(Shooter, MainInstigator);
}

void AExplosive::Explode(AActor* Shooter, AController* MainInstigator)
{
	bDestroyed = true;
	TArray<AActor*> OverlappingActors;
	GetOverlappingActors(OverlappingActors);

	for (AActor* Actor : OverlappingActors)
	{
		if (AExplosive* Explosive = Cast<AExplosive>(Actor))
		{
			if (Explosive->bDestroyed == false)
			{
				Explosive->Explode(Shooter, MainInstigator);
			}
		}
		if (MainInstigator && Cast<ACharacter>(Actor))
		{
			UGameplayStatics::ApplyDamage(Actor, ExplosionDamage, MainInstigator, Shooter, UDamageType::StaticClass());
		}
	}
	Destroy();
}
