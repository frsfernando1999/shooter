// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCharacter.h"

#include "Ammo.h"
#include "BulletHitInterface.h"
#include "Enemy.h"
#include "EnemyController.h"
#include "Item.h"
#include "Weapon.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/WidgetComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Engine/SkeletalMeshSocket.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
AMainCharacter::AMainCharacter() :
	BaseTurnRate(45.0f),
	BaseLookUpRate(45.0f),
	TurnRateAiming(20.f),
	LookUpRateAiming(20.f),
	TurnRateHip(80.f),
	LookUpRateHip(80.f),
	bAiming(false),
	CameraDefaultFOV(0.f),
	CameraZoomedFOV(75.f),
	ZoomInterpolationSpeed(20.f),
	CrosshairVelocityFactor(0.f),
	CrosshairInAirFactor(0.f),
	CrosshairAimFactor(0.f),
	CrosshairShootingFactor(0.f),
	ShootTimeDuration(0.05f),
	bFireButtonPressed(false),
	bShouldFire(true),
	CameraInterpDistance(250.f),
	CameraInterpElevation(65.f),
	Starting9mmAmmo(85),
	StartingARAmmo(120),
	CombatState(ECombatState::ECS_Unoccupied),
	bCrouching(false),
	BaseMovementSpeed(600.f),
	CrouchMovementSpeed(300.f),
	StandingCapsuleHalfHeight(88.f),
	CrouchingCapsuleHalfHeight(44.f),
	BaseGroundFriction(2.f),
	CrouchBaseFriction(100.f),
	bAimingButtonPressed(false),
	bShouldPlayPickupSound(true),
	bShouldPlayEquipSound(true),
	PickupSoundResetTime(0.2f),
	EquipSoundResetTime(0.2f),
	HighlightedSlot(-1),
	Health(200.f),
	MaxHealth(200.f),
	StunChance(0.05f)

{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera Boom"));
	CameraBoom->SetupAttachment(RootComponent);
	//CameraBoom->SocketOffset = FVector(0.f, 50.f, 50.f);

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 540.f, 0.f);
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2F;

	HandSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("HandSceneComponent"));

	WeaponInterpComp = CreateDefaultSubobject<USceneComponent>(TEXT("WeaponInterpolationComponent"));
	WeaponInterpComp->SetupAttachment(Camera);

	InterpComp1 = CreateDefaultSubobject<USceneComponent>(TEXT("InterpolationComponent1"));
	InterpComp1->SetupAttachment(Camera);

	InterpComp2 = CreateDefaultSubobject<USceneComponent>(TEXT("InterpolationComponent2"));
	InterpComp2->SetupAttachment(Camera);

	InterpComp3 = CreateDefaultSubobject<USceneComponent>(TEXT("InterpolationComponent3"));
	InterpComp3->SetupAttachment(Camera);

	InterpComp4 = CreateDefaultSubobject<USceneComponent>(TEXT("InterpolationComponent4"));
	InterpComp4->SetupAttachment(Camera);

	InterpComp5 = CreateDefaultSubobject<USceneComponent>(TEXT("InterpolationComponent5"));
	InterpComp5->SetupAttachment(Camera);

	InterpComp6 = CreateDefaultSubobject<USceneComponent>(TEXT("InterpolationComponent6"));
	InterpComp6->SetupAttachment(Camera);
}

float AMainCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
                                 AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (Health - DamageAmount <= 0.f)
	{
		Health = 0;
		
		FTimerHandle DeathTimer;
		CombatState = ECombatState::ECS_Stunned;
		GetWorldTimerManager().SetTimer(DeathTimer, this, &AMainCharacter::Die, 0.2f, false);
		
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Ignore);

		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Ignore);
		
		const AEnemyController* EnemyController = Cast<AEnemyController>(EventInstigator);
		if (EnemyController)
		{
			EnemyController->GetBlackboardComponent()->SetValueAsBool(FName("PlayerDead"), true);
			EnemyController->GetBlackboardComponent()->SetValueAsBool(FName("Target"), nullptr);
		}

	}
	else
	{
		Health -= DamageAmount;
	}
	return DamageAmount;
}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();
	if (Camera)
	{
		CameraDefaultFOV = Camera->FieldOfView;
		CameraCurrentFOV = CameraDefaultFOV;
	}
	EquipWeapon(SpawnDefaultWeapon());
	Inventory.Add(EquippedWeapon);

	EquippedWeapon->SetSlotIndex(0);
	EquippedWeapon->DisableGlowMaterial();
	EquippedWeapon->DisableCustomDepth();

	InitializeAmmoMap();

	GetCharacterMovement()->MaxWalkSpeed = BaseMovementSpeed;
	InitializeInterpLocations();
}

void AMainCharacter::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);

	ChangeCameraFOV(DeltaTime);
	SetAimingSensitivity();
	CalculateCrosshairSpread(DeltaTime);
	TraceForItems();
	InterpCapsuleHalfHeight(DeltaTime);
}

void AMainCharacter::ChangeCameraFOV(const float DeltaTime)
{
	if (bAiming)
	{
		CameraCurrentFOV = FMath::FInterpTo(CameraCurrentFOV, CameraZoomedFOV, DeltaTime, ZoomInterpolationSpeed);
	}
	else
	{
		CameraCurrentFOV = FMath::FInterpTo(CameraCurrentFOV, CameraDefaultFOV, DeltaTime, ZoomInterpolationSpeed);
	}
	Camera->SetFieldOfView(CameraCurrentFOV);
}

void AMainCharacter::SetAimingSensitivity()
{
	if (bAiming)
	{
		BaseTurnRate = TurnRateAiming;
		BaseLookUpRate = LookUpRateAiming;
	}
	else
	{
		BaseTurnRate = TurnRateHip;
		BaseLookUpRate = LookUpRateHip;
	}
}

void AMainCharacter::CalculateCrosshairSpread(float DeltaTime)
{
	CrosshairVelocityFactor = GetVelocityFactor();
	CrosshairInAirFactor = GetCrosshairAirFactor(DeltaTime);
	CrosshairAimFactor = GetCrosshairAimFactor(DeltaTime);
	CrosshairShootingFactor = GetCrosshairShootingFactor(DeltaTime);

	CrosshairSpreadMultiplier = 0.5f
		+ CrosshairVelocityFactor
		+ CrosshairInAirFactor
		+ CrosshairAimFactor
		+ CrosshairShootingFactor;
}

void AMainCharacter::TraceForItems()
{
	if (bShouldTraceForItems)
	{
		FHitResult ItemTraceResult;
		TraceUnderCrosshair(ItemTraceResult);
		if (ItemTraceResult.bBlockingHit)
		{
			TraceHitItem = Cast<AItem>(ItemTraceResult.GetActor());

			if (const AWeapon* TraceHitWeapon = Cast<AWeapon>(TraceHitItem))
			{
				if (HighlightedSlot == -1)
				{
					HighlightInventorySlot();
				}
			}
			else
			{
				if (HighlightedSlot != -1)
				{
					UnHighlightInventorySlot();
				}
			}

			if (TraceHitItem && TraceHitItem->GetItemState() == EItemState::EIS_EquipInterping)
			{
				TraceHitItem = nullptr;
			}
			if (TraceHitItem && TraceHitItem->GetPickupWidget())
			{
				TraceHitItem->GetPickupWidget()->SetVisibility(true);
				TraceHitItem->EnableCustomDepth();

				if (Inventory.Num() >= INVENTORY_CAPACITY)
				{
					TraceHitItem->SetCharacterInventoryFull(true);
				}
				else
				{
					TraceHitItem->SetCharacterInventoryFull(false);
				}
			}
			if (LastTargetedItem)
			{
				if (LastTargetedItem != TraceHitItem)
				{
					LastTargetedItem->GetPickupWidget()->SetVisibility(false);
					LastTargetedItem->DisableCustomDepth();
				}
			}
			LastTargetedItem = TraceHitItem;
		}
	}
	else if (LastTargetedItem)
	{
		LastTargetedItem->GetPickupWidget()->SetVisibility(false);
		LastTargetedItem->DisableCustomDepth();
	}
}

bool AMainCharacter::GetCrosshairPositionAndDirection(FVector& OutCrosshairWorldPosition,
                                                      FVector& OutCrosshairWorldDirection) const
{
	FVector2D ViewportSize;
	if (GEngine && GEngine->GameViewport)
	{
		GEngine->GameViewport->GetViewportSize(ViewportSize);
	}

	const FVector2D CrosshairLocation(ViewportSize.X / 2.f, ViewportSize.Y / 2.f);


	return UGameplayStatics::DeprojectScreenToWorld(
		UGameplayStatics::GetPlayerController(this, 0),
		CrosshairLocation,
		OutCrosshairWorldPosition,
		OutCrosshairWorldDirection);
}

void AMainCharacter::Fire()
{
	if (EquippedWeapon == nullptr) return;
	if (CombatState != ECombatState::ECS_Unoccupied) return;

	if (WeaponHasAmmo())
	{
		PlayFireSound();
		SendBullet();
		PlayGunfireMontage();
		EquippedWeapon->DecrementAmmo();

		StartFireTimer();
		if (EquippedWeapon->GetWeaponType() == EWeaponType::EWT_Pistol)
		{
			EquippedWeapon->StartSlideTimer();
		}
	}
}

void AMainCharacter::PlayGunfireMontage()
{
	// Play Hip Fire Montage
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && HipFireMontage)
	{
		AnimInstance->Montage_Play(HipFireMontage);
		AnimInstance->Montage_JumpToSection(FName("StartFire"));
	}

	// Start bullet fire timer for crosshairs
	StartCrosshairBulletFire();
}


void AMainCharacter::PlayFireSound()
{
	// Play fire sound
	if (EquippedWeapon->GetFireSound())
	{
		UGameplayStatics::PlaySound2D(this, EquippedWeapon->GetFireSound());
	}
}


void AMainCharacter::StartFireTimer()
{
	if (EquippedWeapon == nullptr) return;

	CombatState = ECombatState::ECS_FireTimerInProgress;
	GetWorldTimerManager().SetTimer(AutoFireTimer, this, &AMainCharacter::AutoFireReset,
	                                EquippedWeapon->GetAutoFireRate());
}

void AMainCharacter::AutoFireReset()
{
	if (CombatState == ECombatState::ECS_Stunned) return;

	CombatState = ECombatState::ECS_Unoccupied;
	if (EquippedWeapon == nullptr) return;

	if (WeaponHasAmmo())
	{
		if (bFireButtonPressed && EquippedWeapon->GetAutomatic())
		{
			Fire();
		}
	}
	else
	{
		ReloadWeapon();
	}
}

void AMainCharacter::SendBullet()
{
	// Send bullet
	const USkeletalMeshSocket* BarrelSocket =
		EquippedWeapon->GetItemMesh()->GetSocketByName("barrelSocket");
	if (BarrelSocket)
	{
		const FTransform SocketTransform = BarrelSocket->GetSocketTransform(
			EquippedWeapon->GetItemMesh());

		if (EquippedWeapon->GetMuzzleFlash())
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), EquippedWeapon->GetMuzzleFlash(), SocketTransform);
		}

		FHitResult SmokeTrailResult;
		bool bBeamEnd = GetSmokeTrailEndLocation(
			SocketTransform.GetLocation(), SmokeTrailResult);
		if (bBeamEnd)
		{
			if (SmokeTrailResult.GetActor())
			{
				if (IBulletHitInterface* BulletHitInterface = Cast<IBulletHitInterface>(SmokeTrailResult.GetActor()))
				{
					BulletHitInterface->BulletHit_Implementation(SmokeTrailResult, this, GetController());
					if (AEnemy* HitEnemy = Cast<AEnemy>(SmokeTrailResult.GetActor()))
					{
						if (SmokeTrailResult.BoneName.ToString() == HitEnemy->GetHeadBone())
						{
							UGameplayStatics::ApplyDamage(SmokeTrailResult.GetActor(),
							                              EquippedWeapon->GetHeadshotDamage(),
							                              GetController(),
							                              this,
							                              UDamageType::StaticClass());
							HitEnemy->ShowDamageNumber(EquippedWeapon->GetHeadshotDamage(), SmokeTrailResult.Location,
							                           true);
						}
						else
						{
							UGameplayStatics::ApplyDamage(SmokeTrailResult.GetActor(),
							                              EquippedWeapon->GetDamage(),
							                              GetController(),
							                              this,
							                              UDamageType::StaticClass());
							HitEnemy->ShowDamageNumber(EquippedWeapon->GetDamage(), SmokeTrailResult.Location, false);
						}
					}
				}

				else
				{
					if (ImpactParticles)
					{
						UGameplayStatics::SpawnEmitterAtLocation(
							GetWorld(),
							ImpactParticles,
							SmokeTrailResult.Location);
					}
				}
			}

			UParticleSystemComponent* SmokeTrail = UGameplayStatics::SpawnEmitterAtLocation(
				GetWorld(),
				BulletSmokeTrail,
				SocketTransform);
			if (SmokeTrail)
			{
				SmokeTrail->SetVectorParameter(FName("Target"), SmokeTrailResult.Location);
			}
		}
	}
}

bool AMainCharacter::GetSmokeTrailEndLocation(
	const FVector& MuzzleSocketLocation,
	FHitResult& OutHitResult)
{
	FVector OutBeamLocation;
	FHitResult CrosshairHitResult;
	bool bCrosshairHit = TraceUnderCrosshairs(CrosshairHitResult, OutBeamLocation);

	if (bCrosshairHit)
	{
		OutBeamLocation = CrosshairHitResult.Location;
	}


	const FVector WeaponTraceStart{MuzzleSocketLocation};
	const FVector StartToEnd{OutBeamLocation - WeaponTraceStart};
	const FVector WeaponTraceEnd{MuzzleSocketLocation + StartToEnd * 1.25f};
	GetWorld()->LineTraceSingleByChannel(
		OutHitResult,
		WeaponTraceStart,
		WeaponTraceEnd,
		ECC_Visibility);
	if (!OutHitResult.bBlockingHit)
	{
		OutHitResult.Location = OutBeamLocation;
		return false;
	}

	return true;
}

bool AMainCharacter::TraceUnderCrosshairs(
	FHitResult& OutHitResult,
	FVector& OutHitLocation)
{
	FVector2D ViewportSize;
	if (GEngine && GEngine->GameViewport)
	{
		GEngine->GameViewport->GetViewportSize(ViewportSize);
	}

	FVector2D CrosshairLocation(ViewportSize.X / 2.f, ViewportSize.Y / 2.f);
	FVector CrosshairWorldPosition;
	FVector CrosshairWorldDirection;

	bool bScreenToWorld = UGameplayStatics::DeprojectScreenToWorld(
		UGameplayStatics::GetPlayerController(this, 0),
		CrosshairLocation,
		CrosshairWorldPosition,
		CrosshairWorldDirection);

	if (bScreenToWorld)
	{
		const FVector Start{CrosshairWorldPosition};
		const FVector End{Start + CrosshairWorldDirection * 50'000.f};
		OutHitLocation = End;
		GetWorld()->LineTraceSingleByChannel(
			OutHitResult,
			Start,
			End,
			ECollisionChannel::ECC_Visibility);
		if (OutHitResult.bBlockingHit)
		{
			OutHitLocation = OutHitResult.Location;
			return true;
		}
	}
	return false;
}


float AMainCharacter::GetVelocityFactor() const
{
	const FVector2D WalkSpeedRange{0.f, 600.f};
	const FVector2D VelocityMultiplierRange{0.f, 1.f};

	FVector Velocity{GetVelocity()};

	Velocity.Z = 0.f;

	return FMath::GetMappedRangeValueClamped(WalkSpeedRange, VelocityMultiplierRange, Velocity.Size());
}

float AMainCharacter::GetCrosshairAirFactor(float DeltaTime) const
{
	if (GetCharacterMovement()->IsFalling())
	{
		return FMath::FInterpTo(CrosshairInAirFactor, 2.25f, DeltaTime, 2.25f);
	}
	return FMath::FInterpTo(CrosshairInAirFactor, 0.f, DeltaTime, 30.f);
}


float AMainCharacter::GetCrosshairAimFactor(float DeltaTime) const
{
	if (bAiming)
	{
		return FMath::FInterpTo(CrosshairAimFactor, -0.5, DeltaTime, 30.f);
	}
	return FMath::FInterpTo(CrosshairAimFactor, 0.5, DeltaTime, 30.f);
}

float AMainCharacter::GetCrosshairShootingFactor(float DeltaTime) const
{
	if (bFiringBullet)
	{
		return FMath::FInterpTo(CrosshairShootingFactor, 0.3f, DeltaTime, 60.f);
	}
	return FMath::FInterpTo(CrosshairShootingFactor, 0.0f, DeltaTime, 60.f);
}

void AMainCharacter::StartCrosshairBulletFire()
{
	bFiringBullet = true;

	GetWorldTimerManager().SetTimer(CrosshairShootTimer,
	                                this,
	                                &AMainCharacter::FinishCrosshairBulletFire,
	                                ShootTimeDuration);
}

void AMainCharacter::FinishCrosshairBulletFire()
{
	bFiringBullet = false;
}

bool AMainCharacter::TraceUnderCrosshair(FHitResult& OutHitResult) const
{
	FVector CrosshairWorldPosition;
	FVector CrosshairWorldDirection;

	if (GetCrosshairPositionAndDirection(CrosshairWorldPosition, CrosshairWorldDirection))
	{
		const FVector Start{CrosshairWorldPosition};
		const FVector End{Start + CrosshairWorldDirection * 50'000.f};
		GetWorld()->LineTraceSingleByChannel(OutHitResult, Start, End, ECC_Visibility);
		if (OutHitResult.bBlockingHit)
		{
			return true;
		}
	}

	return false;
}

float AMainCharacter::GetCrosshairSpreadMultiplier() const
{
	return CrosshairSpreadMultiplier;
}

void AMainCharacter::IncrementOverlappedItemCount(int8 Amount)
{
	if (OverlappedItemCount + Amount <= 0)
	{
		OverlappedItemCount = 0;
		bShouldTraceForItems = false;
	}
	else
	{
		OverlappedItemCount += Amount;
		bShouldTraceForItems = true;
	}
}

void AMainCharacter::GetPickupItem(AItem* Item)
{
	Item->PlayEquipSound();

	AWeapon* Weapon = Cast<AWeapon>(Item);
	if (Weapon)
	{
		if (Inventory.Num() < INVENTORY_CAPACITY)
		{
			Weapon->SetSlotIndex(Inventory.Num());
			Inventory.Add(Weapon);
			Weapon->SetItemState(EItemState::EIS_PickedUp);
		}
		else // Inventory is full! Swap with EquippedWeapon
		{
			SwapWeapon(Weapon);
		}
	}

	auto Ammo = Cast<AAmmo>(Item);
	if (Ammo)
	{
		PickupAmmo(Ammo);
	}
}

FInterpLocation AMainCharacter::GetInterpLocation(int32 Index)
{
	if (Index <= InterpLocations.Num())
	{
		return InterpLocations[Index];
	}
	return FInterpLocation();
}

void AMainCharacter::StartPickupSoundTimer()
{
	bShouldPlayPickupSound = false;
	GetWorldTimerManager().SetTimer(
		PickupSoundTimer,
		this,
		&AMainCharacter::ResetPickupSoundTimer,
		PickupSoundResetTime);
}

void AMainCharacter::StartEquipSoundTimer()
{
	bShouldPlayEquipSound = false;
	GetWorldTimerManager().SetTimer(
		EquipSoundTimer,
		this,
		&AMainCharacter::ResetEquipSoundTimer,
		EquipSoundResetTime);
}

void AMainCharacter::ResetPickupSoundTimer()
{
	bShouldPlayPickupSound = true;
}

void AMainCharacter::ResetEquipSoundTimer()
{
	bShouldPlayEquipSound = true;
}

AWeapon* AMainCharacter::SpawnDefaultWeapon() const
{
	if (DefaultWeaponClass)
	{
		return GetWorld()->SpawnActor<AWeapon>(DefaultWeaponClass);
	}
	return nullptr;
}

void AMainCharacter::EquipWeapon(AWeapon* WeaponToEquip)
{
	if (WeaponToEquip)
	{
		const USkeletalMeshSocket* HandSocket = GetMesh()->GetSocketByName(FName("right_hand_socket"));
		if (HandSocket)
		{
			HandSocket->AttachActor(WeaponToEquip, GetMesh());
		}

		if (EquippedWeapon == nullptr)
		{
			EquipItemDelegate.Broadcast(-1, WeaponToEquip->GetSlotIndex());
		}
		else
		{
			EquipItemDelegate.Broadcast(EquippedWeapon->GetSlotIndex(), WeaponToEquip->GetSlotIndex());
		}

		EquippedWeapon = WeaponToEquip;

		EquippedWeapon->SetItemState(EItemState::EIS_Equipped);
	}
}

void AMainCharacter::DropWeapon()
{
	if (EquippedWeapon)
	{
		FDetachmentTransformRules DetachmentTransformRules(EDetachmentRule::KeepWorld, true);
		EquippedWeapon->GetItemMesh()->DetachFromComponent(DetachmentTransformRules);

		EquippedWeapon->SetItemState(EItemState::EIS_Falling);
		EquippedWeapon->ThrowWeapon();
	}
}

void AMainCharacter::SwapWeapon(AWeapon* WeaponToSwap)
{
	if (Inventory.Num() - 1 >= EquippedWeapon->GetSlotIndex())
	{
		Inventory[EquippedWeapon->GetSlotIndex()] = WeaponToSwap;
		WeaponToSwap->SetSlotIndex(EquippedWeapon->GetSlotIndex());
	}

	DropWeapon();
	EquipWeapon(WeaponToSwap);
	TraceHitItem = nullptr;
	LastTargetedItem = nullptr;
}

void AMainCharacter::InitializeAmmoMap()
{
	AmmoMap.Add(EAmmoType::EAT_9mm, Starting9mmAmmo);
	AmmoMap.Add(EAmmoType::EAT_AR, StartingARAmmo);
}

bool AMainCharacter::WeaponHasAmmo() const
{
	if (EquippedWeapon == nullptr) return false;

	return EquippedWeapon->GetAmmo() > 0;
}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AMainCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveSides"), this, &AMainCharacter::MoveSides);

	PlayerInputComponent->BindAxis(TEXT("LookSides"), this, &AMainCharacter::LookSides);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &AMainCharacter::LookUp);

	PlayerInputComponent->BindAxis(TEXT("LookSidesRate"), this, &AMainCharacter::LookSidesRate);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AMainCharacter::LookUpRate);

	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &AMainCharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &AMainCharacter::FireButtonPressed);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &AMainCharacter::FireButtonPressed);

	PlayerInputComponent->BindAction(TEXT("Aim"), IE_Pressed, this, &AMainCharacter::AimingButtonPressed);
	PlayerInputComponent->BindAction(TEXT("Aim"), IE_Released, this, &AMainCharacter::AimingButtonReleased);

	PlayerInputComponent->BindAction(TEXT("Interact"), IE_Pressed, this, &AMainCharacter::InteractButtonPressed);
	PlayerInputComponent->BindAction(TEXT("Interact"), IE_Released, this, &AMainCharacter::InteractButtonReleased);

	PlayerInputComponent->BindAction(TEXT("Reload"), IE_Pressed, this, &AMainCharacter::ReloadButtonPressed);
	PlayerInputComponent->BindAction(TEXT("Reload"), IE_Released, this, &AMainCharacter::ReloadButtonReleased);

	PlayerInputComponent->BindAction(TEXT("Crouch"), IE_Pressed, this, &AMainCharacter::CrouchButtonPressed);
	PlayerInputComponent->BindAction(TEXT("Crouch"), IE_Released, this, &AMainCharacter::CrouchButtonReleased);

	PlayerInputComponent->BindAction(TEXT("FKey"), IE_Pressed, this, &AMainCharacter::FKeyPressed);
	PlayerInputComponent->BindAction(TEXT("1Key"), IE_Pressed, this, &AMainCharacter::OneKeyPressed);
	PlayerInputComponent->BindAction(TEXT("2Key"), IE_Pressed, this, &AMainCharacter::TwoKeyPressed);
	PlayerInputComponent->BindAction(TEXT("3Key"), IE_Pressed, this, &AMainCharacter::ThreeKeyPressed);
	PlayerInputComponent->BindAction(TEXT("4Key"), IE_Pressed, this, &AMainCharacter::FourKeyPressed);
	PlayerInputComponent->BindAction(TEXT("5Key"), IE_Pressed, this, &AMainCharacter::FiveKeyPressed);
}

void AMainCharacter::AimingButtonPressed()
{
	bAimingButtonPressed = true;
	if (CombatState != ECombatState::ECS_Reloading && CombatState != ECombatState::ECS_Equipping && CombatState !=
		ECombatState::ECS_Stunned)
	{
		Aim();
	}
}

void AMainCharacter::AimingButtonReleased()
{
	bAimingButtonPressed = false;
	StopAiming();
}

void AMainCharacter::FireButtonPressed()
{
	bFireButtonPressed = !bFireButtonPressed;
	Fire();
}

void AMainCharacter::InteractButtonPressed()
{
	if (CombatState != ECombatState::ECS_Unoccupied) return;

	if (TraceHitItem)
	{
		TraceHitItem->StartItemCurve(this);
		TraceHitItem = nullptr;
	}
}

void AMainCharacter::InteractButtonReleased()
{
}

void AMainCharacter::ReloadButtonPressed()
{
	ReloadWeapon();
}

void AMainCharacter::ReloadButtonReleased()
{
}

bool AMainCharacter::CarryingAmmo()
{
	if (EquippedWeapon == nullptr) return false;
	EAmmoType AmmoType = EquippedWeapon->GetAmmoType();

	if (AmmoMap.Contains(AmmoType))
	{
		return AmmoMap[AmmoType] > 0;
	}
	return false;
}

void AMainCharacter::ReloadAnimation() const
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (ReloadMontage && AnimInstance)
	{
		AnimInstance->Montage_Play(ReloadMontage);
		AnimInstance->Montage_JumpToSection(EquippedWeapon->GetReloadMontageSection());
	}
}

void AMainCharacter::ReloadWeapon()
{
	if (CombatState != ECombatState::ECS_Unoccupied) return;
	if (EquippedWeapon == nullptr) return;
	if (CarryingAmmo() && EquippedWeapon->MagazineIsFull() == false)
	{
		CombatState = ECombatState::ECS_Reloading;
		ReloadAnimation();
		if (bAiming)
		{
			StopAiming();
		}
	}
}

void AMainCharacter::FinishReloadingWeapon()
{
	if (CombatState == ECombatState::ECS_Stunned) return;

	CombatState = ECombatState::ECS_Unoccupied;

	if (bAimingButtonPressed)
	{
		Aim();
	}
	if (EquippedWeapon == nullptr) return;

	const EAmmoType AmmoType = EquippedWeapon->GetAmmoType();

	if (AmmoMap.Contains(AmmoType))
	{
		int32 CarriedAmmo = AmmoMap[AmmoType];

		const int32 MagEmptySpace = EquippedWeapon->GetMagazineCapacity() - EquippedWeapon->GetAmmo();

		if (MagEmptySpace > CarriedAmmo)
		{
			EquippedWeapon->ReloadAmmo(CarriedAmmo);
			CarriedAmmo = 0;
			AmmoMap.Add(AmmoType, CarriedAmmo);
		}
		else
		{
			EquippedWeapon->ReloadAmmo(MagEmptySpace);
			CarriedAmmo -= MagEmptySpace;
			AmmoMap.Add(AmmoType, CarriedAmmo);
		}
	}
}

void AMainCharacter::FinishEquipWeapon()
{
	if (CombatState == ECombatState::ECS_Stunned) return;

	CombatState = ECombatState::ECS_Unoccupied;
	if (bAimingButtonPressed)
	{
		Aim();
	}
}

void AMainCharacter::GrabMagazine()
{
	if (EquippedWeapon == nullptr) return;
	if (HandSceneComponent == nullptr) return;
	const int32 MagazineBoneIndex{EquippedWeapon->GetItemMesh()->GetBoneIndex(EquippedWeapon->GetMagazineBoneName())};
	MagazineTransform = EquippedWeapon->GetItemMesh()->GetBoneTransform(MagazineBoneIndex);

	FAttachmentTransformRules AttachmentRules(EAttachmentRule::KeepRelative, true);
	HandSceneComponent->AttachToComponent(GetMesh(), AttachmentRules, FName(TEXT("hand_l")));
	HandSceneComponent->SetWorldTransform(MagazineTransform);
	EquippedWeapon->SetMovingMagazine(true);
}

void AMainCharacter::ReplaceMagazine()
{
	EquippedWeapon->SetMovingMagazine(false);
}

void AMainCharacter::MoveForward(const float AxisValue)
{
	if (Controller && AxisValue != 0.0f)
	{
		const FRotator Rotation{Controller->GetControlRotation()};
		const FRotator YawRotation{0, Rotation.Yaw, 0};

		const FVector Direction{FRotationMatrix{YawRotation}.GetUnitAxis(EAxis::X)};
		AddMovementInput(Direction, AxisValue);
	}
}

void AMainCharacter::MoveSides(const float AxisValue)
{
	if (Controller && AxisValue != 0.0f)
	{
		const FRotator Rotation{Controller->GetControlRotation()};
		const FRotator YawRotation{0, Rotation.Yaw, 0};

		const FVector Direction{FRotationMatrix{YawRotation}.GetUnitAxis(EAxis::Y)};
		AddMovementInput(Direction, AxisValue);
	}
}

void AMainCharacter::LookUpRate(float Value)
{
	APawn::AddControllerPitchInput(Value * GetWorld()->GetDeltaSeconds() * BaseLookUpRate);
}

void AMainCharacter::LookSidesRate(float Value)
{
	APawn::AddControllerYawInput(Value * GetWorld()->GetDeltaSeconds() * BaseTurnRate);
}

void AMainCharacter::LookSides(float Value)
{
	APawn::AddControllerYawInput(Value * (BaseTurnRate / 100));
}

void AMainCharacter::LookUp(float Value)
{
	APawn::AddControllerPitchInput(Value * (BaseLookUpRate / 100));
}

void AMainCharacter::CrouchButtonPressed()
{
	if (GetCharacterMovement()->IsFalling() == false)
	{
		bCrouching = !bCrouching;
	}
	if (bCrouching)
	{
		GetCharacterMovement()->MaxWalkSpeed = CrouchMovementSpeed;
		GetCharacterMovement()->GroundFriction = CrouchBaseFriction;
	}
	else
	{
		GetCharacterMovement()->MaxWalkSpeed = BaseMovementSpeed;
		GetCharacterMovement()->GroundFriction = BaseGroundFriction;
	}
}

void AMainCharacter::CrouchButtonReleased()
{
}

void AMainCharacter::Jump()
{
	if (bCrouching)
	{
		bCrouching = false;
		GetCharacterMovement()->MaxWalkSpeed = BaseMovementSpeed;
	}
	else
	{
		ACharacter::Jump();
	}
}

void AMainCharacter::InterpCapsuleHalfHeight(float DeltaTime) const
{
	float TargetCapsuleHalfHeight;
	if (bCrouching)
	{
		TargetCapsuleHalfHeight = CrouchingCapsuleHalfHeight;
	}
	else
	{
		TargetCapsuleHalfHeight = StandingCapsuleHalfHeight;
	}
	const float InterpHalfHeight = FMath::FInterpTo(
		GetCapsuleComponent()->GetScaledCapsuleHalfHeight(),
		TargetCapsuleHalfHeight,
		DeltaTime,
		20.f);
	const float DeltaCapsuleHalfHeight = InterpHalfHeight - GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	const FVector MeshOffset{0.f, 0.f, -DeltaCapsuleHalfHeight};
	GetMesh()->AddLocalOffset(MeshOffset);
	GetCapsuleComponent()->SetCapsuleHalfHeight(InterpHalfHeight);
}

void AMainCharacter::Aim()
{
	bAiming = true;
	GetCharacterMovement()->MaxWalkSpeed = CrouchMovementSpeed;
}

void AMainCharacter::StopAiming()
{
	bAiming = false;
	if (!bCrouching)
	{
		GetCharacterMovement()->MaxWalkSpeed = BaseMovementSpeed;
	}
}

void AMainCharacter::FKeyPressed()
{
	if (EquippedWeapon->GetSlotIndex() == 0) return;

	ExchangeInventoryItems(EquippedWeapon->GetSlotIndex(), 0);
}

void AMainCharacter::OneKeyPressed()
{
	if (EquippedWeapon->GetSlotIndex() == 1) return;

	ExchangeInventoryItems(EquippedWeapon->GetSlotIndex(), 1);
}

void AMainCharacter::TwoKeyPressed()
{
	if (EquippedWeapon->GetSlotIndex() == 2) return;

	ExchangeInventoryItems(EquippedWeapon->GetSlotIndex(), 2);
}

void AMainCharacter::ThreeKeyPressed()
{
	if (EquippedWeapon->GetSlotIndex() == 3) return;

	ExchangeInventoryItems(EquippedWeapon->GetSlotIndex(), 3);
}

void AMainCharacter::FourKeyPressed()
{
	if (EquippedWeapon->GetSlotIndex() == 4) return;

	ExchangeInventoryItems(EquippedWeapon->GetSlotIndex(), 4);
}

void AMainCharacter::FiveKeyPressed()
{
	if (EquippedWeapon->GetSlotIndex() == 5) return;

	ExchangeInventoryItems(EquippedWeapon->GetSlotIndex(), 5);
}

void AMainCharacter::ExchangeInventoryItems(int32 CurrentItemIndex, int32 NewItemIndex)
{
	if (bAiming)
	{
		StopAiming();
	}

	if (CurrentItemIndex != NewItemIndex &&
		NewItemIndex < Inventory.Num() &&
		CombatState == ECombatState::ECS_Unoccupied)
	{
		AWeapon* OldEquippedWeapon = EquippedWeapon;
		AWeapon* NewWeapon = Cast<AWeapon>(Inventory[NewItemIndex]);
		EquipWeapon(NewWeapon);

		OldEquippedWeapon->SetItemState(EItemState::EIS_PickedUp);
		NewWeapon->SetItemState(EItemState::EIS_Equipped);

		CombatState = ECombatState::ECS_Equipping;
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance && EquipMontage)
		{
			AnimInstance->Montage_Play(EquipMontage);
			AnimInstance->Montage_JumpToSection(FName("Equip"));
		}
	}
}

int32 AMainCharacter::GetEmptyInventorySlot()
{
	for (int32 i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i] == nullptr)
		{
			return i;
		}
	}

	if (Inventory.Num() < INVENTORY_CAPACITY)
	{
		return Inventory.Num();
	}

	return -1;
}

void AMainCharacter::HighlightInventorySlot()
{
	const int32 EmptySlot = GetEmptyInventorySlot();

	HighlightIconDelegate.Broadcast(EmptySlot, true);
	HighlightedSlot = EmptySlot;
}

EPhysicalSurface AMainCharacter::GetSurfaceType()
{
	// Add PhysicsCore to Dependency Modules
	FHitResult HitResult;
	const FVector End{GetActorLocation() + FVector(0.f, 0.f, -100.f)};
	FCollisionQueryParams QueryParams;
	QueryParams.bReturnPhysicalMaterial = true;

	GetWorld()->LineTraceSingleByChannel(HitResult, GetActorLocation(), End, ECC_Visibility, QueryParams);

	return UPhysicalMaterial::DetermineSurfaceType(HitResult.PhysMaterial.Get());
}

void AMainCharacter::EndStun()
{
	CombatState = ECombatState::ECS_Unoccupied;

	if (bAimingButtonPressed)
	{
		Aim();
	}
}

FName AMainCharacter::GetRandomMontageSection()
{
	switch (UKismetMathLibrary::RandomInteger(3))
	{
	case 0:
		return FName("HitReactFront");
	case 1:
		return FName("HitReactBack");
	case 2:
		return FName("HitReactRight");
	case 3:
		return FName("HitReactLeft");
	default:
		return FName("HitReactBack");
	}
}

void AMainCharacter::Die()
{
	if (APlayerController* CharacterController = Cast<APlayerController>(GetController()))
	{
		DisableInput(CharacterController);
	}
	if (DeathMontage)
	{
		if (UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance())
		{
			AnimInstance->Montage_Play(DeathMontage);
		}
	}
}

void AMainCharacter::FinishDeath()
{
	GetMesh()->bPauseAnims = true;
	APlayerController* PC = UGameplayStatics::GetPlayerController(this, 0);
}

void AMainCharacter::UnHighlightInventorySlot()
{
	HighlightIconDelegate.Broadcast(HighlightedSlot, false);
	HighlightedSlot = -1;
}

void AMainCharacter::Stun()
{
	if (Health <= 0.f) return;

	CombatState = ECombatState::ECS_Stunned;

	if (bAiming)
	{
		StopAiming();
	}

	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && HitReactMontage)
	{
		AnimInstance->Montage_Play(HitReactMontage);
		AnimInstance->Montage_JumpToSection(GetRandomMontageSection(), HitReactMontage);
	}
}


void AMainCharacter::PickupAmmo(AAmmo* Ammo)
{
	const EAmmoType AmmoType = Ammo->GetAmmoType();
	if (AmmoMap.Find(AmmoType))
	{
		int32 AmmoCount = AmmoMap[AmmoType];
		AmmoCount += Ammo->GetItemCount();
		AmmoMap[AmmoType] = AmmoCount;
	}

	if (EquippedWeapon->GetAmmoType() == AmmoType)
	{
		if (EquippedWeapon->GetAmmo() == 0)
		{
			ReloadWeapon();
		}
	}

	Ammo->Destroy();
}

void AMainCharacter::InitializeInterpLocations()
{
	const FInterpLocation WeaponLocation{WeaponInterpComp, 0};
	InterpLocations.Add(WeaponLocation);

	const FInterpLocation InterpLoc1{InterpComp1, 0};
	InterpLocations.Add(InterpLoc1);

	const FInterpLocation InterpLoc2{InterpComp2, 0};
	InterpLocations.Add(InterpLoc2);

	const FInterpLocation InterpLoc3{InterpComp3, 0};
	InterpLocations.Add(InterpLoc3);

	const FInterpLocation InterpLoc4{InterpComp4, 0};
	InterpLocations.Add(InterpLoc4);

	const FInterpLocation InterpLoc5{InterpComp5, 0};
	InterpLocations.Add(InterpLoc5);

	const FInterpLocation InterpLoc6{InterpComp6, 0};
	InterpLocations.Add(InterpLoc6);
}


int32 AMainCharacter::GetInterpLocationIndex()
{
	// Weapon is Index 0
	int32 LowestIndex = 1;
	int32 LowestCount = INT_MAX;
	for (int32 i = 1; i < InterpLocations.Num(); i++)
	{
		if (InterpLocations[i].ItemCount < LowestCount)
		{
			LowestIndex = i;
			LowestCount = InterpLocations[i].ItemCount;
		}
	}
	return LowestIndex;
}

void AMainCharacter::IncrementInterpLocItemCount(int32 Index, int32 Amount)
{
	if (Amount < -1 || Amount > 1) return;
	if (InterpLocations.Num() >= Index)
	{
		InterpLocations[Index].ItemCount += Amount;
	}
}
