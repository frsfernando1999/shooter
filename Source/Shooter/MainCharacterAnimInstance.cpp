// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCharacterAnimInstance.h"

#include "MainCharacter.h"
#include "Weapon.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"

UMainCharacterAnimInstance::UMainCharacterAnimInstance():
	Speed(0.f),
	bIsAirborne(false),
	bIsAccelerating(false),
	MovementOffsetStrafing(0.f),
	LastMovementOffsetStrafing(0.f),
	bAiming(false),
	TurnInPlaceCharacterYaw(0.f),
	TurnInPlaceCharacterYawLastFrame(0.f),
	RootYawOffset(0.f),
	Pitch(0.f),
	bReloading(false),
	CharacterRotation(FRotator(0.f)),
	CharacterRotationLastFrame(FRotator(0.f)),
	YawDelta(0.f),
	RecoilWeight(0.f),
	bTurningInPlace(false),
	EquippedWeaponType(EWeaponType::EWT_MAX),
	bShouldUseFABRIK(false)
{
}

void UMainCharacterAnimInstance::UpdateAnimationProperties(float DeltaTime)
{
	if (MainCharacter == nullptr)
	{
		MainCharacter = Cast<AMainCharacter>(TryGetPawnOwner());
	}

	if (MainCharacter)
	{
		const ECombatState CharacterCurrentState = MainCharacter->GetCombatState();
		
		bCrouching = MainCharacter->GetCrouching();
		bReloading = CharacterCurrentState == ECombatState::ECS_Reloading;
		bEquipping = CharacterCurrentState == ECombatState::ECS_Equipping;
		bShouldUseFABRIK = CharacterCurrentState == ECombatState::ECS_Unoccupied || CharacterCurrentState == ECombatState::ECS_FireTimerInProgress;

		FVector Velocity{MainCharacter->GetVelocity()};
		Velocity.Z = 0;
		Speed = Velocity.Size();

		bIsAirborne = MainCharacter->GetCharacterMovement()->IsFalling();

		bIsAccelerating = MainCharacter->GetCharacterMovement()->GetCurrentAcceleration().Size() > 0.f;

		const FRotator AimRotation = MainCharacter->GetBaseAimRotation();
		const FRotator MovementRotation = UKismetMathLibrary::MakeRotFromX(MainCharacter->GetVelocity());
		MovementOffsetStrafing = UKismetMathLibrary::NormalizedDeltaRotator(MovementRotation, AimRotation).Yaw;
		if (MainCharacter->GetVelocity().Size() > 0.f)
		{
			LastMovementOffsetStrafing = MovementOffsetStrafing;
		}
		bAiming = MainCharacter->GetAiming();
		if (bReloading)
		{
			OffsetState = EOffsetState::EOS_Reloading;
		}
		else if (bIsAirborne)
		{
			OffsetState = EOffsetState::EOS_InAir;
		}
		else if (MainCharacter->GetAiming())
		{
			OffsetState = EOffsetState::EOS_Aiming;
		}
		else
		{
			OffsetState = EOffsetState::EOS_Hip;
		}
		if (MainCharacter->GetEquippedWeapon())
		{
			EquippedWeaponType = MainCharacter->GetEquippedWeapon()->GetWeaponType();
		}
	}
	TurnInPlace();
	Lean(DeltaTime);
}

void UMainCharacterAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	MainCharacter = Cast<AMainCharacter>(TryGetPawnOwner());
}

void UMainCharacterAnimInstance::TurnInPlace()
{
	if (MainCharacter == nullptr) return;

	Pitch = MainCharacter->GetBaseAimRotation().Pitch;

	if (Speed > 0 || bIsAirborne)
	{
		RootYawOffset = 0.f;
		TurnInPlaceCharacterYaw = MainCharacter->GetActorRotation().Yaw;
		TurnInPlaceCharacterYawLastFrame = TurnInPlaceCharacterYaw;
		RotationCurveLastFrame = 0.f;
		RotationCurve = 0.f;
	}
	else
	{
		TurnInPlaceCharacterYawLastFrame = TurnInPlaceCharacterYaw;
		TurnInPlaceCharacterYaw = MainCharacter->GetActorRotation().Yaw;

		const float TurnInPlaceYawDelta{TurnInPlaceCharacterYaw - TurnInPlaceCharacterYawLastFrame};
		//Clamps Between [-180, 180]
		RootYawOffset = UKismetMathLibrary::NormalizeAxis(RootYawOffset - TurnInPlaceYawDelta);

		const float Turning{GetCurveValue(TEXT("Turning"))};
		if (Turning > 0)
		{
			bTurningInPlace = true;
			RotationCurveLastFrame = RotationCurve;
			RotationCurve = GetCurveValue(TEXT("Rotation"));
			const float DeltaRotation{RotationCurve - RotationCurveLastFrame};
			//RootYawOffset > 0 -> Turn Left
			//RootYawOffset < 0 -> Turn Right
			RootYawOffset > 0 ? RootYawOffset -= DeltaRotation : RootYawOffset += DeltaRotation;

			const float ABSRootYawOffset{FMath::Abs(RootYawOffset)};
			if (ABSRootYawOffset > 90.f)
			{
				const float YawExcess{ABSRootYawOffset - 90.f};
				RootYawOffset > 0 ? RootYawOffset -= YawExcess : RootYawOffset += YawExcess;
			}
		}
		else
		{
			bTurningInPlace = false;
		}
	}
	if (bTurningInPlace)
	{
		if (bReloading || bEquipping)
		{
			RecoilWeight = 1.f;
		}
		else
		{
			RecoilWeight = 0.f;
		}
	}
	else
	{
		if (bCrouching)
		{
			if (bReloading || bEquipping)
			{
				RecoilWeight = 1.f;
			}
			else
			{
				RecoilWeight = 0.1f;
			}
		}
		else
		{
			if (bAiming || bReloading || bEquipping)
			{
				RecoilWeight = 1.f;
			}
			else
			{
				RecoilWeight = 0.5f;
			}
		}
	}
}

void UMainCharacterAnimInstance::Lean(float DeltaTime)
{
	if (MainCharacter == nullptr) return;
	CharacterRotationLastFrame = CharacterRotation;
	CharacterRotation = MainCharacter->GetActorRotation();

	const FRotator Delta{UKismetMathLibrary::NormalizedDeltaRotator(CharacterRotation, CharacterRotationLastFrame)};

	const float Target = Delta.Yaw / DeltaTime;

	const float Interp{FMath::FInterpTo(YawDelta, Target, DeltaTime, 6.f)};

	YawDelta = FMath::Clamp(Interp, -90.f, 90.f);
}
